---
title: Other writing directions
layout: default
nav_order: 50
parent: Future Improvement Ideas
permalink: /future-improvement-ideas/writing-directions.html
---

# Other writing directions

It's outside of the scope of this document to address writing directions
other than LTR or RTL, e.g. TTB (top to bottom).

Mixed use of let's say both horizontal and vertical implicit text
present at the same time would require magnitudes more work than just
BiDi, and there's no common need for that.

If it's sufficient to pick a *line orientation* and *line progression*
(as per the terminology used by ECMA) on a global basis, that is,
retroactively applying let's say top-down writing direction to the
entire contents, then supporting terminals and applications should
implement ECMA's SPD sequence.

Details of such modes, e.g. rotating some glyphs, mirroring the
mirrorable BiDi characters, transforming the box drawing characters,
handling of scrollback buffer, shuffling of the arrow keys etc. would be
subject to further research.
