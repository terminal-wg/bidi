---
title: Other cursor placement strategies
layout: default
nav_order: 30
parent: Future Improvement Ideas
permalink: /future-improvement-ideas/cursor-strategies.html
---

# Other cursor placement strategies

Handling the cursor is quite tricky in input fields of graphical
applications when it comes to BiDi, due to the fact that the BiDi
algorithm doesn't map the boundaries. There is one more possible place
for the cursor than characters in the row, so one of the positions has
to have special treatment. There doesn't seem to be a consensus on how
it should work. For example:

- In GTK+'s one-line entry fields, the arrow keys move the cursor in
  the visual string. I haven't studied how it exactly maps to logical
  position, that is, how subsequent insertion or deletion behaves. (In
  all the other apps below the arrow keys move the cursor in the
  logical string.)
- In Google Docs, for BiDi the cursor belongs to the *following*
  logical letter and is displayed at that letter's *start* edge. (This
  is inconsistent with the display of attributes such as bold, italic,
  font, font size etc. in the toolbar; there the *preceding* letter's
  properties are shown.) When the cursor is at the end of the logical
  string, it's shown at the visual end of the line. This is weird
  while typing a foreign direction word at the end of a line, the
  cursor is not where one types to.
- In KDE's (Qt's) one-line entry fields, the cursor also belongs to
  the *following* logical letter and is displayed at its *start* edge.
  When the cursor is at the end of the logical string, it's shown at
  the *end* of the last logical letter. This is better for typing a
  foreign direction word than Google Docs, at least as long as there
  isn't any subsequent character, not even a trailing space after the
  cursor. An invisible trailing space after the cursor effectively
  reverts the behavior to Google Docs's one which might be confusing.
- In LibreOffice Writer, the cursor belongs to the *preceding* letter
  and is displayed at its *end* edge. The special case is when the
  cursor is at the beginning of the line, then it's displayed at the
  *start* edge of the first logical letter. When entering new text, it
  provides the Qt-like experience of the cursor being after the
  just-typed letter, even when there are trailing whitespaces.
- Gedit splits the cursor and maps it according to both the *preceding
  and following* letter, that is, at the logical boundary of regular
  and foreign text you have two visual cursors, one with a tiny arrow
  pointing to the left and another one with a tiny arrow to the right.

For implicit mode, the safest bet (the most compatible approach
presumably causing the least amount of surprise) is to follow the
underlying letter and display the cursor over it, on the side
corresponding to the character's resolved direction. With I-beam shape
this results in Google Docs's behavior, and this is the mode that I
recommended in "The cursed cursor" chapter.

If the need arises for other cursor placement strategies in *implicit*
mode, potentially for the I-beam shape only, we could introduce new
escape sequences for them, probably as new numeric parameters to
DECSCUSR.

For *explicit* mode, if the need arises, we may introduce a way to
explicitly specify the side of the cursor, and/or whether to display
there some arrow.
