---
title: RTL UI handling in general
layout: default
nav_order: 20
parent: RTL and BiDi Introduction
permalink: /bidi-intro/rtl-ui.html
---

# RTL UI handling in general

In all well designed systems, an overall RTL user interface is just a
*display* property. Let's take an example: with English settings your
graphical application consists of three columns, from left to right: 1,
2, 3. You probably build up an HTML DOM, or a widget hierarchy with them
being the children of their container in this order.

Whatever contents you have in the left column, you want that to be in
the right column in RTL locales, and vice versa. The visual order should
be 3 on the left, 2 in the center, 1 on the right. However, the logical
order is still 1, 2, 3 (your eyes should scan the screen from right to
left), and so should be in your software, in the DOM or widget
hierarchy. The application's business logic shouldn't require any
change. It's solely a display property telling the browser or the
graphical toolkit to lay them out in opposite order.

(Story: About 10 years ago, in HTML/CSS an element could have a
background image aligned to the left, to the right, or to the left with
an additional offset. It was not possible to align it to the right with
an additional offset. If an LTR-only webpage used left alignment with an
offset, it couldn't easily be mirrored to RTL, a different technical
solution had to be chosen (involving modifications to the DOM, CSS and
JS code). This limitation was soon after fixed in CSS.)

Those who RTL-ify a terminal based application should also be able to
easily follow this principle. They should be able to add RTL support as
a display-only property, not affecting the business logic. The only way
to do that is if we make terminal emulators fully symmetrical for the
two display directions.
