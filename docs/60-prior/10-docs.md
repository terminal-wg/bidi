---
title: Docs
layout: default
nav_order: 10
parent: Prior Work
permalink: /prior-work/docs.html
---

# Docs
{:.no_toc}

* TOC
{:toc}

## ECMA TR/53

ECMA Technical Report TR/53, plus ECMA 48 for the exact escape sequences
are the only papers I could find about how BiDi in terminal emulators
should work. I devote a separate chapter to them.

## Arabeyes

[Arabeyes](https://www.arabeyes.org/) is mentioned from PuTTY's
changelog. Doesn't seem to be active nowadays.

Their
[ArabeyesTodo](https://www.arabeyes.org/ArabeyesTodo#Terminal_Emulators)
page says:

> "Need an overall spec on how bidi/control-characters are to be handled
> within a terminal emulator (cursor movement, etc)"

Funny to see that apparently they went ahead and implemented something
for PuTTY without any, and the bullet points for implementing it in
other emulators also precede the bullet point of having a spec.
