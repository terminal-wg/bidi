---
title: Terminal emulators
layout: default
nav_order: 20
parent: Prior Work
permalink: /prior-work/terminals.html
---

# Terminal emulators
{:.no_toc}

* TOC
{:toc}

## Common problems

I could test three emulators on Linux that claim to support BiDi (KDE's
Konsole, Mlterm and PuTTY's Pterm), and received screenshots of macOS's
default Terminal.app. All of these four suffer from these problems:

There is no way to turn off BiDi (that is, switch to explicit mode), at
least not using the standard ECMA escape sequences. As I've proved it
earlier, this makes it literally impossible to create fullscreen text
viewers and editors on top of these platforms.

They all run the BiDi algorithm on lines rather than paragraphs. With
autodetection this pretty often gives incorrect result, e.g. if an
embedded word of foreign direction happens to cross line boundary, the
entire next line is laid out in opposite base direction. Example (with
the right edge emphasized):

    this one here is an english sentence containing a RBEH▏
                 .word and then some more english words WE▏

Whereas at a slightly different window size where the foreign direction
word doesn't cross the line boundary, it's all as expected:

    this one here is an english sentence containing a WERBEH w▏
    ord and then some more english words.                     ▏

Without autodetection this particular use case is handled correctly,
however, more complex examples might still easily cause faulty layout,
including faulty mirroring.

## PuTTY

I'm testing pterm version 0.70 as shipped by Ubuntu 18.10. According to
its changelog, BiDi support was added in version 0.58 in 2004--2005. No
followup fixes or improvements mentioned in the changelog.

As per [PuTTY semi-bug
bidi](https://www.chiark.greenend.org.uk/~sgtatham/putty/wishlist/bidi.html)
they know that:

> "Proper support for this will need to involve a properly thought out
> design"

It removes all BiDi control characters, then applies BiDi on the rest.
Hence it implements *implicit mode level 1*.

It does autodetection on the text for its line direction (and alignment
as well). So it actually implements "implicit mode with autodetection"
only.

In addition to the common bugs mentioned above, I encountered the
following issue:

`echo '12345 HEBREW'` produces buggy result (with the right edge of the
terminal emphasized here):

    54321                                              WERBEH ▏

The number is reversed, there's a whitespace at the right margin, and
aligning at two sides at once also shouldn't happen.

## Konsole

I'm testing version 18.04.3 as shipped by Ubuntu 18.10.

Removes all BiDi control characters, then applies BiDi on the rest.
Hence it implements *implicit mode level 1* (apparently implicit LTR,
without autodetection).

In addition to the common bugs mentioned above, I encountered the
following issue:

Applies BiDi on continuous runs of identical attributes. Any change in
e.g. color (or even highlight with the mouse, or the cursor being
positioned inside) stops and starts it anew, often resulting in a
confusing and incorrect visual behavior.

I believe Konsole doesn't do any BiDi itself, just passes continuous
regions of text with identical attributes to some font rendering method
which does.

## Mlterm

I'm testing version 3.8.6, as shipped by Ubuntu 18.10.

Handles BiDi control characters (sounds like *implicit mode level 2*).
Not sure how exactly.

Has command line options `--bimode=normal/left/right` without further
explanation. `normal` does autodetection, `left` seems to strictly
left-align everything, however, `right` still does some autodetection
and left-aligns certain things, including lines that contain box drawing
characters. Doesn't make sense to me.

In addition to the common bugs mentioned above, I encountered the
following issues:

Huge visual corruptions. With combinations of non-ASCII LTR or box
drawing characters and RTL ones, it's quite easy to produce output that
gets painted elsewhere or none at all upon a mouse highlight, or
multiple glyphs are painted over each other. The experience is utterly
buggy and inconsistent, sometimes even causing brand new characters
(that weren't present in the stream at all) to appear. Smells like bad
code that can even corrupt the memory.

Resizing the window causes sudden jumps, e.g. many previously
right-aligned lines become left-aligned, BiDi control characters also
disappear.

Highlighting with the mouse selects a continuous visual region, rather
than a continuous logical one. Bizarre, not the typical behavior of
BiDi-aware graphical apps.

## Terminal.app

I could not test it, just received some screenshots confirming that it
does some essential BiDi (apparently implicit LTR, at least level 1,
without autodetection).

It suffers from the common bugs mentioned above.

## Bicon

I only looked at it to the extent that it allows typing BiDi text, but
breaks when you backspace. (It's *much* harder to do BiDi when your
backend is a terminal emulator rather than a graphical canvas.)

I'm pretty certain that this specification supersedes Bicon.
