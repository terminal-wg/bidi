---
title: How the escape sequences apply
layout: default
nav_order: 20
parent: Rationale and Dropped Ideas
permalink: /rationale-and-dropped-ideas/how-the-escape-sequences-apply.html
---

# How the escape sequences apply

On what levels should BiDi-related escape sequences apply?

On the entire contents: This would potentially result in output of
earlier commands getting mangled by subsequent ones. Clearly wrong.

On paragraphs: This is what we went for.

On lines: Clearly worse than tracking it for paragraphs. The BiDi
algorithm operates on paragraphs, not lines. How would a paragraph's
explicit or implicit mode, or direction be derived from its lines? How
would lines receive these properties after the terminal emulator reflows
its contents on a resize?

On character cells: This seems a reasonable idea to consider. A bit more
wasteful than the per-paragraph version, in the sense that each cell
would need to store a couple of new bits. The advantage is that we
wouldn't need to come up with a new strategy for tracking per-paragraphs
properties. We'd need to figure out how to derive the paragraph's
properties from the cells, maybe take them from the first cell. The
biggest problem is: even empty paragraphs, ones that consist of erased
cells only, have to have a well-defined value for these properties (e.g.
the direction matters for cursor placement or mouse reporting). How
would we assign values to these erased cells? The only attribute they
can currently take is the background color which is extremely broken,
see the ["bce madness" VTE
issue](https://bugzilla.gnome.org/show\_bug.cgi?id=754596), we shouldn't
follow that broken practice, we'd need to come up with something new for
tracking the BiDi properties on them. Plus, erased cells appear and
disappear as the terminal is resized, no matter if the contents are
rewrapped or not. Since we don't have an out of the box good solution
for these questions and we'd need to come up with something new anyway,
I found it cleaner to answer these kinds of questions for paragraphs
straight away.

Why do they apply immediately on a paragraph when emitted at the
paragraph's first position?

Utilities (especially in implicit mode) might just wait for user input,
without displaying a prompt. Examples include a simple `read` or `read
-e` statement in a shell script, or utilities like `bc -q`, `sort` etc.
used interactively. Before they're executed, a BiDi escape sequence
(perhaps from a wrapper script) might want to influence the overall
direction (initial side where the cursor appears) and subsequently the
display of the input typed to these.

Why don't they apply immediately on a paragraph when emitted at a
subsequent position?

First, I see no reason why any app would first emit some content and
define its BiDi properties later on. It's semantically much cleaner to
require them to define the properties first. Second, terminal emulators
often convert the scrollback data into some other, perhaps read-only
format. The top of a paragraph might have already scrolled out while its
bottom is still writable. Being able to change properties of already
scrolled out data could cause an unnecessary implementation complexity
to certain emulators.

Yet another approach could be to apply the new mode if the cursor is
anywhere in the paragraph's first line. It's a bit arbitrary, and I'm
afraid it would eventually lead to some software that just prints stuff
using implicit mode, sets certain modes somewhere in the middle or at
the end of the line, and fails to realize that it breaks if the terminal
window is made too narrow. With content rewrapping on resize, the
output's look should ideally be agnostic to the width at the time the
output was produced, it should only depend on the current width;
applying BiDi controls received in the first line but not in subsequent
lines would break this principle.
