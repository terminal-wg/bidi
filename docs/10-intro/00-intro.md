---
title: Introduction
layout: default
nav_order: 10
permalink: /
---

# Introduction
{:.no_toc}

* TOC
{:toc}

## Foreword

Handling right-to-left (RTL) or bi-directional (BiDi) text is an often
requested feature for various terminal emulators. As I'll show later, I
am not satisfied with any existing documentation or implementation I
could find in this topic. Therefore I decided to come up with a new
proposal (built on top of prior work by others), and implement it in the
VTE terminal emulation widget.

I aim to come up with a design that is clean, simple, practical, follows
best practices, and is easily implementable by terminal emulators and
terminal-based applications.

Some areas I deliberately leave open for the time being. By implementing
the basics in a few key emulators and apps, we'll gather real life
experience for the missing bits. Ideally this specification will receive
subsequent versions that are (reasonably) backwards compatible with
earlier ones.

This specification is still work in progress. Especially during the
early days of adoption, it might receive backward incompatible updates.
The topic of BiDi in terminals has been unsolved for decades, I'd much
rather have occasional breakages during the first few years of adoption
than carry a faulty decision and its consequences for decades to come.
In the (hopefully unlikely) event of spotting a bug or bad design, the
specification will be fixed and implementations will be expected to
adjust reasonably quickly.

## Conventions in this document

### Fake RTL

It's a standard convention in examples to use lowercase letters for LTR
and uppercase English letters for "fake RTL", since obviously readers of
this document and developers of relevant software aren't expected to
read any of these scripts or speak any of these languages. Example:

    this is an english sentence ending in SDROW WERBEH EMOS.

ECMA TR/53 uses opposite casing.

Whenever I say "English", as well as "Arabic" or "Hebrew" (pretty
randomly) throughout the document (except when speaking of Arabic
shaping), please treat them as "any LTR script" and "any RTL script".

### Applications, utilities

Whenever I say "application" ("app" for short) or "utility", unless
otherwise stated or otherwise obvious from context, I refer to an
application or utility running *inside* the terminal emulator (e.g.
bash, cat, mc, vim, emacs, tmux, cowsay... you name it). I tend to use
"utility" for simpler ones and "application" for the fullscreen ones
(that is, using the entire canvas of the terminal emulator).

### Terminal, emulator

I use "terminal", "emulator" and "terminal emulator" interchangeably for
the very same thing. It's typical loose wording to say "terminal" when
it refers to a graphical emulator and not a hardware one.
