---
title: Erased cells
layout: default
nav_order: 40
parent: The Recommendation
permalink: /recommendation/erased-cells.html
---

# Erased cells

Most terminal emulators distinguish *erased* (this is the ECMA-48
terminology; can also be called *unused* or *empty*) cells from spaces,
e.g. they don't copy-paste anything from there. The screen initially
consists of erased cells, they appear at the bottom when scrolling, they
appear on the escape sequence that clears to the end of the line etc.

In cooked mode (e.g. when typing to `cat`), when the user enters
something but then erases it by pressing Backspace, technically the
terminal emulator receives space characters to overwrite the previous
ones. That is, initially erased cells get replaced by spaces.

Luckily, the BiDi algorithm places trailing logical whitespaces to the
trailing side of the rendered string. That is, when shuffling the cells
according to the BiDi algorithm, this difference of erased cells vs.
space characters remains invisible to the user.

In both *implicit* and *explicit* modes, erased cells are mapped to the
end (according to the paragraph direction) of the line, in the order
according to this direction. (This matters e.g. if the cursor is there,
or the mouse is clicked.) For example: if the terminal is 80 characters
wide, a line has RTL direction, and in this line the first 60 cells of
the model contain actual characters and the last 20 cells are erased,
then the first 60 cells are mapped somehow to the rightmost 60 columns,
the model's 61st cell is mapped to the visual 61st cell from the right
(20th from the left), the model's last (80th) cell is mapped to the
leftmost column.

Trailing erased cells cannot only occur at the end of paragraphs, they
can also occur at end of lines mid-paragraph. One example is when the
window is made wider and the emulator doesn't rewrap the lines; it
probably adds erased cells there. Another example is when a double wide
character didn't fit in the last column, so had to be wrapped to the
next line, leaving a single erased cell at the end of the previous line.
Such erased cells should be ignored when feeding the paragraph's
contents to the BiDi algorithm (just as they should be ignored when
copy-pasting, searching etc.), and should also be skipped when rendering
the result (so that each character remains in its row, and still no wide
character is cut in half).
